package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class CreateMovieMapper {

  public Movie mapToEntity(CreateMovieDto dto) {
    final Movie movie = new Movie();
    movie.setTitle(dto.getTitle());
    movie.setImage(dto.getImage());
    movie.setYear(dto.getYear());

    return movie;
  }
}
